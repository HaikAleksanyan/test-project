# springboot-test-app

For building and running the application you need:

* Java 11
* PostgreSQL 13
* Gradle 6.8

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.creativeweb.test.TestApplication.java` class from your IDE.

make sure that you postgres is running
```shell
gradle bootRun
```
with docker-compose 
1.make sure that your local machine got docker and docker-compose
2.inside project root directory run
```shell
docker-compose up
```
for running tests try to run `com/creativeweb/test/TestApplicationTests.java` class from your local IntellijIdea