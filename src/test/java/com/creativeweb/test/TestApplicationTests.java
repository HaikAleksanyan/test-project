package com.creativeweb.test;

import com.creativeweb.test.repository.CommentRepository;
import com.creativeweb.test.service.CommentCreatingDto;
import com.creativeweb.test.service.CommentPreviewDto;
import com.creativeweb.test.service.NotificationPreviewDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@ActiveProfiles("Test")
@RunWith(SpringRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = TestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestApplicationTests {
    private static int successfullyComCount = 0;
    private static int successfullyNotificationCount = 0;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private CommentRepository commentRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private String endpointUrl(String func) {
        return "http://localhost:" + port + "/comment" + func;
    }

    @RepeatedTest(value = 100)
    public void saveComment_AndCheck_IfCommentAndNotification_ExistInList(RepetitionInfo repetitionInfo) throws JSONException, JsonProcessingException, URISyntaxException {
        String comment = "I am " + repetitionInfo.getCurrentRepetition() + "th comment";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        // save comment
        ResponseEntity<String> response = restTemplate.postForEntity(new URI(endpointUrl("/save")), new CommentCreatingDto(comment), String.class);

        Long commentId = null;
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            commentId = Long.valueOf(Objects.requireNonNull(response.getBody()));
        }

        Long notificationId = null;

        // get comments
        response = restTemplate.exchange(endpointUrl("/?page=0&size=1000"), HttpMethod.GET, entity, String.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            List<CommentPreviewDto> commentList = objectMapper.readValue(new JSONObject(response.getBody()).get("content").toString(), new TypeReference<>() {
            });
            final Long finalCommentId = commentId;
            // check if comment added in list
            if (commentId != null) {
                Optional<CommentPreviewDto> optionalComment = commentList.stream().filter(c -> c.getId() == finalCommentId).findFirst();
                if (optionalComment.isPresent()) {
                    notificationId = optionalComment.get().getNotificationId();
                    successfullyComCount++;
                }
            }
        }

        // get notifications from api
        response = restTemplate.exchange(endpointUrl("/getNotifications?page=0&size=1000"), HttpMethod.GET, entity, String.class);

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            List<NotificationPreviewDto> notificationList = objectMapper.readValue(new JSONObject(response.getBody()).get("content").toString(), new TypeReference<>() {
            });

            if (notificationId != null) {
                final long finalNotificationId = notificationId;
                Optional<NotificationPreviewDto> optionalNotification = notificationList.stream().filter(n -> n.getId() == finalNotificationId).findFirst();

                // check if notification added in list and delivered
                if (optionalNotification.isPresent() && optionalNotification.get().isDelivered()) {
                    successfullyNotificationCount++;
                }
            }
        }

        System.out.println("Test No." + repetitionInfo.getCurrentRepetition() + "/" + repetitionInfo.getTotalRepetitions() + " finished");
    }

    @AfterAll
    public void showStatisticsAndClearDb() {
        System.out.println("\n\n\n");
        System.out.println("Successfully added comments:          " + (successfullyComCount == 0 ? 0 : (successfullyComCount * 100) / 100) + "%");
        System.out.println("Successfully delivered notifications: " + (successfullyNotificationCount == 0 ? 0 : (successfullyNotificationCount * 100) / 100) + "%");
        System.out.println("\n\n\n");
        commentRepository.deleteAll();
    }
}