package com.creativeweb.test.entity;

import com.creativeweb.test.service.NotificationPreviewDto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notification_tbl")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date time;
    private boolean delivered;

    @OneToOne
    @JoinColumn(name = "comment_id", referencedColumnName = "id", nullable = false)
    private Comment comment;

    public Notification(){}

    public Notification(Comment comment, boolean delivered) {
        this.delivered = delivered;
        this.time = new Date();
        this.comment = comment;
    }

    public NotificationPreviewDto toPreview(){
        return new NotificationPreviewDto(this.id, this.delivered, this.time, this.comment.getId());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}