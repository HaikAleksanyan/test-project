package com.creativeweb.test.entity;

import com.creativeweb.test.service.CommentPreviewDto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comment_tbl")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "comment", nullable = false)
    private String comment;

    private Date time;

    @OneToOne(mappedBy = "comment", targetEntity = Notification.class, cascade = CascadeType.ALL)
    private Notification notification;

    public Comment(){
    }

    public Comment(final String comment) {
        this.time = new Date();
        this.comment = comment;
    }

    public CommentPreviewDto toPreview(){
        return new CommentPreviewDto(this.id, this.comment, this.time, this.notification.getId());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Notification getNotification() {
        return notification;
    }
}