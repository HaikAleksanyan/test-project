package com.creativeweb.test.controller;

import com.creativeweb.test.service.CommentCreatingDto;
import com.creativeweb.test.service.CommentPreviewDto;
import com.creativeweb.test.service.CommentService;
import com.creativeweb.test.service.NotificationPreviewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("comment/")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("save")
    public long save(@RequestBody CommentCreatingDto comment) {
        return commentService.saveComment(comment);
    }

    @GetMapping()
    public Page<CommentPreviewDto> getComments(@RequestParam int page, @RequestParam int size) {
        return commentService.getComments(PageRequest.of(page,size));
    }

    @GetMapping("getNotifications")
    public Page<NotificationPreviewDto> getNotifications(@RequestParam int page, @RequestParam int size) throws InterruptedException {
        Thread.sleep(2000);
        return commentService.getNotifications(PageRequest.of(page,size));
    }
}