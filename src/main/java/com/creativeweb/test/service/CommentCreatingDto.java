package com.creativeweb.test.service;


public class CommentCreatingDto {
    private String comment;

    public CommentCreatingDto() {
    }

    public CommentCreatingDto(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}