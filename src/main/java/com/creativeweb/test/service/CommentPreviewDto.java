package com.creativeweb.test.service;

import java.util.Date;

public class CommentPreviewDto {
    private long id;
    private String comment;
    private Date time;
    private long notificationId;

    public CommentPreviewDto(long id, String comment, Date time, long notificationId) {
        this.id = id;
        this.comment = comment;
        this.time = time;
        this.notificationId = notificationId;
    }
    public CommentPreviewDto(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }
}