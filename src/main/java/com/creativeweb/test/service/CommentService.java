package com.creativeweb.test.service;

import com.creativeweb.test.entity.Comment;
import com.creativeweb.test.entity.Notification;
import com.creativeweb.test.repository.CommentRepository;
import com.creativeweb.test.repository.NotificationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class CommentService {
    private final NotificationRepository notificationRepository;
    private final CommentRepository commentRepository;

    public CommentService(NotificationRepository notificationRepository, CommentRepository commentRepository) {
        this.notificationRepository = notificationRepository;
        this.commentRepository = commentRepository;
    }

    @Transactional
    public long saveComment(final CommentCreatingDto dto) {
        Comment comment = commentRepository.save(new Comment(dto.getComment()));
        saveNotification(comment);
        BusinessLogic.doSomeWorkOnCommentCreation();
        return comment.getId();
    }

    private void saveNotification(final Comment comment) {
        boolean delivered = true;

        try {
            BusinessLogic.doSomeWorkOnNotification();
        } catch (RuntimeException exception) {
            delivered = false;
        }
        notificationRepository.saveAndFlush(new Notification(comment, delivered));
    }

    public Page<CommentPreviewDto> getComments(Pageable pageable) {
        return commentRepository.findAll(pageable).map(Comment::toPreview);
    }

    public Page<NotificationPreviewDto> getNotifications(Pageable pageable) {
        return notificationRepository.findAll(pageable).map(Notification::toPreview);
    }
}