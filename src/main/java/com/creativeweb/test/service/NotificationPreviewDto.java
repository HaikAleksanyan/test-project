package com.creativeweb.test.service;

import java.util.Date;

public class NotificationPreviewDto {
    private long id;
    private boolean delivered;
    private Date time;
    private long commentId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public NotificationPreviewDto(long id, boolean delivered, Date time, long commentId) {
        this.id = id;
        this.delivered = delivered;
        this.time = time;
        this.commentId = commentId;
    }
    public NotificationPreviewDto(){}
}